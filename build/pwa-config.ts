import { VitePWA } from 'vite-plugin-pwa'

export function initPluginPWA() {
  return VitePWA({
    manifest: {
      name: '404Coding - 更适合前端的上网首页！',
      short_name: '404Coding',
    },
    registerType: 'autoUpdate',
    injectRegister: 'inline',
    workbox: {
      globPatterns: ['**/*.{icon,png,svg}', '**/*.{js,css,html}', '**/*.json'],
      maximumFileSizeToCacheInBytes: 1024 * 1024 * 10,
    },
  })
}