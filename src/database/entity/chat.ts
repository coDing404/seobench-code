export interface ChatMessage {
    user: string;
    type: string;
    content: string;
}


export class Chat {
    messages: ChatMessage[] = []; 
    newMessage: string = '';

    parse() {
        return {
            messages: this.messages,
            newMessage: this.newMessage
        }
    }
}

  