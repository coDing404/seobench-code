import { getIDBRequest } from '../db'
import { ChatMessage } from '@database/entity/chat'
import { list, add } from '../manager/chat-manager'

export async function getMsgList() {
    const db: IDBDatabase = await getIDBRequest()
    return await list(db)
}

export async function addMsg(chat: ChatMessage) {
    const db: IDBDatabase = await getIDBRequest()
    return await add(db,chat)
}

