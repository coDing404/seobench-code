import * as Cron from 'cron-parser';

export function getNextCronTime(cronExpression: string) {
  const parser = Cron.parseExpression(cronExpression);
  const nextDate = new Date(parser.next()._date.ts);
  return nextDate;
}


export function getNextTenCronTime(cronExpression: string) {
  const parser = Cron.parseExpression(cronExpression);
  const nextDateArr = [];
  nextDateArr.push(parser.prev().toDate()) 
  for (let i = 0; i < 10; i++) {
    const nextDate = new Date(parser.next()._date.ts);
    nextDateArr.push(nextDate)
 }
  return nextDateArr;
}

