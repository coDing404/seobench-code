/**
 * 分割字符串，以第一个冒号为分隔符
 * @param input - 要分割的字符串
 * @returns 一个包含两个字符串的数组，第一个字符串是冒号之前的部分，第二个字符串是冒号之后的部分
 */
export function splitInFirstColon(input: string): [string, string] {
  const output = input.match(/(^[^:]+)(?:\:(.+))*/)
  if (!output) {
    return ['', '']
  }
  return [output[1] || '', output[2] || '']
}

/**
 * 检查字符串是否以指定的字符串开头
 * @param input - 要检查的字符串
 * @param start - 要检查的开头字符串
 * @returns 如果字符串以指定的字符串开头，则返回 true，否则返回 false
 */
export function leftPad(num: number) {
  return (num < 10 ? '0' : '') + num
}

/**
 * 格式化时间戳为指定格式的字符串
 * @param timeStample - 时间戳
 * @returns 格式化后的字符串
 */
export function timeFormat(timeStample: number) {
  const date = new Date()
  date.setTime(timeStample)
  return date.getFullYear() + '-' + leftPad(date.getMonth() + 1) + '-' + leftPad(date.getDate()) +
    ' ' +
    leftPad(date.getHours()) + ':' + leftPad(date.getMinutes()) + ':' + leftPad(date.getSeconds())
}


export function getFormattedCurrentTime(): string {
  const now = new Date();
  const year = now.getFullYear();
  const month = (now.getMonth() + 1).toString().padStart(2, '0');
  const day = now.getDate().toString().padStart(2, '0');
  const hours = now.getHours().toString().padStart(2, '0');
  const minutes = now.getMinutes().toString().padStart(2, '0');
  const seconds = now.getSeconds().toString().padStart(2, '0');
  return `${year}${month}${day}${hours}${minutes}${seconds}`;
}



/**
 * 解析 JSON 字符串，如果解析失败则返回空对象
 * @param input - 要解析的 JSON 字符串
 * @returns 解析后的对象
 */
export function jsonParse(input: string) {
  try {
    return JSON.parse(input)
  } catch (e) {
    return {}
  }
}

/**
 * 将对象转换为 JSON 字符串，如果转换失败则返回空字符串
 * @param input - 要转换的对象
 * @returns 转换后的 JSON 字符串
 */
export function jsonStringify(input: any) {
  try {
    return JSON.stringify(input)
  } catch (e) {
    return ''
  }
}