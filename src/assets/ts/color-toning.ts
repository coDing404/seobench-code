function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
  return componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function interpolateColor(color1, color2, factor) {
  if (arguments.length < 3) {
    factor = 0.5;
  }
  const result = color1.slice();
  for (var i = 0; i < 3; i++) {
    result[i] = Math.round(result[i] + factor * (color2[i] - color1[i]));
  }
  return rgbToHex(result[0], result[1], result[2]);
}

// 调色方法
export function interpolateColors(color1: string, color2: string, steps: number) {
  let stepFactor = 1 / (steps - 1);
  let interpolatedColorArray = [];
  let color11 = toRGB(color1.replaceAll('#', '')).map(Number);
  let color21 = toRGB(color2).map(Number);
  for (var i = 0; i < steps; i++) {
    interpolatedColorArray.push(interpolateColor(color11, color21, stepFactor * i))
  }
  return interpolatedColorArray;
}

function toRGB(color: string) {
  let r = parseInt(color.substring(0, 2), 16)
  let g = parseInt(color.substring(2, 4), 16)
  let b = parseInt(color.substring(4, 6), 16)
  let result = [r, g, b];
  return result;
}


// 浅色调
console.log(JSON.stringify(interpolateColors('#3287C8', 'ffffff', 3)))
// 灰色的阴影
console.log(JSON.stringify(interpolateColors('#3287C8', '808080', 3)))
// 黑暗的阴影
console.log(JSON.stringify(interpolateColors('#3287C8', '000000', 3)))