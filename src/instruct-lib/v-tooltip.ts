// tooltip.js
import { DirectiveBinding } from 'vue';
 
const Tooltip = {
  beforeMount(el: HTMLElement, binding: DirectiveBinding) {
    const tooltipText = binding.value;
    const tooltip = document.createElement('div');
    tooltip.textContent = tooltipText;
    tooltip.style.position = 'absolute';
    tooltip.style.background = 'white'; // 修改背景颜色为白色
    tooltip.style.color = 'black'; // 修改文字颜色为黑色
    tooltip.style.borderRadius = '30px'; // 添加圆角
    tooltip.style.marginBottom = '50px';
    tooltip.style.fontSize = '12px';
    tooltip.style.padding = '2px';
    tooltip.style.display = 'none';
    tooltip.style.zIndex = '100';
    tooltip.classList.add('tooltip'); // 添加一个类名以便后续样式调整
 
    el.style.position = 'relative';
    el.appendChild(tooltip);
 
    el.addEventListener('mouseenter', () => {
      tooltip.style.display = 'block';
    });
 
    el.addEventListener('mouseleave', () => {
      tooltip.style.display = 'none';
    });
  }
};
 
export default Tooltip;