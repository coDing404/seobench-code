import axios from 'axios';
import { getAppConfigItem,setAppConfigItem } from '@/assets/ts/app-config'
import {ServiceConfig} from './servicePath';

// 配置新建一个 axios 实例
const service =  axios.create({
	baseURL: ServiceConfig.httpUrl,
	timeout: 50000,
	headers: { 'Content-Type': 'application/json' },
});


// 添加请求拦截器
service.interceptors.request.use((config) => {
		// 在发送请求之前做些什么 token
		const token = getAppConfigItem('token');
		if (token !== 'default') {
			config.headers['Authorization'] = token;
		}

		return config;
	},(error) => {
		// 对请求错误做些什么
		return Promise.reject(error);
	}
);

// 添加响应拦截器
service.interceptors.response.use(
	(response) => {
		// 对响应数据做点什么
		const res = response.data;
		if(res.code == 500 && res.data=='10001'){
			// 清除token
			setAppConfigItem('token', 'default');
            setAppConfigItem('loginName', 'default');
		}
		return response.data;
	},
	(error) => {
		// 对响应错误做点什么

		return Promise.reject(error);
	}
);

// 导出 axios 实例
export default service;
