
import request from "../request";

/**
 * 消息配置 api接口集合
 */
export function messageApi() {

	return {
		// 获取在线情况
		getOnLine: () => {
			return request({
				url: '/message/getOnLine',
				method: 'get',
			});
		},
		// 获取在线情况
		sendSpark: (context: String) => {
			return request({
				url: '/message/sendSpark',
				method: 'post',
				data: { "context": context }
			});
		},



	};
}
