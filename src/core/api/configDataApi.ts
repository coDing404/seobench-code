
import request from "../request";

/**
 * 系统配置 api接口集合
 */
export function configDataApi() {

	return {
		// 获得配置列表
		getData: () => {
			return request({
				url: '/configData',
				method: 'get',
			});
		},

		edit:(data: object) => {
			return request({
				url: '/configData/edit',
				method: 'put',
				data: data,
			});
		},


	};
}
