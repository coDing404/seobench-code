
import request from "../request";

/**
 * 系统配置 api接口集合
 */
export function userApi() {

	return {
		// 登录
		login: (data: object) => {
			return request({
				url: '/user/login',
				method: 'post',
				data: data,
			});
		},

		// 退出
		out:() => {
			return request({
				url: '/user/out',
				method: 'post'
			});
		},
	};
}
