import { EventEmitter } from 'events';//引入events模

const emitter = new EventEmitter();
type Callback = (result: string) => void;

// 定义事件类型
enum seoEventTypeEnum {
    LOGIN = 'login',
    LOGIN_OUT = 'loginOut',
    WEB_SOCKET_CONNECT = 'webSocketConnect',
    WEB_SOCKET_MESSAGE = 'webSocketMessage',
    SPARK_MESSAGE = 'sparkMessage', 
   
}

// 发送事件消息
function seoEventEmit(type: string,data: any) {
  emitter.emit(type, data);
}

// 监听事件消息
function seoEventOn(type: string, callback: Callback) {
    emitter.on(type, function (text) {
        callback(text);
    })
}

// 监听事件消息
function seoEventOnBy(type: string, callback: Callback) {
    emitter.on(type, function (text) {
        callback(text);
    })
}

// 导出
export {
    seoEventEmit,
    seoEventOn,
    seoEventTypeEnum
}