
export enum ServiceConfig {
    // httpUrl = 'https://www.ahzy.top/seobench-api/', 
    httpUrl = 'http://127.0.0.1:9999/seobench/', 
    wsUrl = 'ws://127.0.0.1:9999/seobench/messageSocket/',
    // httpUrl = 'http://121.4.39.49:7004/frp/seobench-api/', 
    // wsUrl = 'ws://121.4.39.49:7004/frp/seobench-api/messageSocket/',
}

export interface Res {
    code: string;
    msg: string;
    data: object;
  }
