# lays-workbench
小剧起始页 [https://e.bh-lay.com/](https://e.bh-lay.com/)

一款轻量的，完全本地化的上网首页，适合前端使用的上网首页，程序员摸鱼起始页。

[这里](http://bh-lay.com/blog?tag=%E5%B0%8F%E5%89%A7%E8%B5%B7%E5%A7%8B%E9%A1%B5)记录了相关的开发经历。

### Prerequisites
- Node & NPM
- node版本

### Install
```sh
npm install
```
### Usage
##### Develop
```sh
# run dev server at localhost:8080
npm run dev
```
##### Build
```sh
# transpile js for deployment
npm run build
```

### 同步代码版本
```sh
https://github.com/bh-lay/lays-workbench
Commits on Mar 6, 2023 d24a59819a3cb73d24e49f49fdf4e2528bd08be9
Commits on Jul 23, 2023 2610620c6b4fbfe73f9ee81a4d9d36b3a79dfc92

/seobench 映射路径
vite.config.ts base 修改
编译前 src/components/public-bookmarks/index.vue iframe的src路径映射
编译后 dist中全局搜索default-data.json 添加全局路径
```